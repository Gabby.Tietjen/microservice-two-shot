from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.http import JsonResponse
from common.json import ModelEncoder
import json



class LocationVoEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style", "fabric", "id", "image_url", "color"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name, "location_href": o.location.import_href}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "image_url",
        "location"
    ]
    encoders = {
        "location": LocationVoEncoder(),
    }



@require_http_methods(["GET","POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET","DELETE", "PUT"])
def api_show_hat(request,pk):
    if request.method == "POST":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
            count, _ = Hat.objects.filter(id=pk).delete()
            return JsonResponse(
                {"deleted": count > 0},
                status=200)
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(name=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"error": "Location does not exist"},
                status=400,
            )



