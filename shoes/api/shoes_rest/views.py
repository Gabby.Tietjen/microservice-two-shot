from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import BinVO, Shoes
from django.http import JsonResponse
import json

# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name", 
        "href", 
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image_url",
        "id",
    ]

    def get_extra_data(self, o):
        return {
            "bin": o.bin.closet_name,
            "bin_href": o.bin.href,
        }


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        #if bin_vo_id is not None:
            #shoes = Shoes.objects.filter(bin=bin_vo_id)
        #else:
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            #id = content["bin"]
            #href = f'/api/bins/{id}/'
            bin = BinVO.objects.get(href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoes(request, pk):
    if request.method == "POST":
        shoes = Shoes.object.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
                count, _ = Shoes.objects.filter(id=pk).delete()
                return JsonResponse(
                    {"deleted": count > 0},
                    status = 200,
                )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(name=content["bin"])
            content["bin"] = bin
        except Shoes.DoesNotExist:
            return JsonResponse(
                {"message": "This does not exist."},
                status = 400,
            )

