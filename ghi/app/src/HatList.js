import React, {useState, useEffect} from "react";


function HatList(props) {
    const [hats, setHats] = useState([])

    useEffect(()=>{
        const getFetch = async () => {
            const url = 'http://localhost:8090/api/hats/';
            const response = await fetch(url);
            const data = await response.json()
            console.log("hat data", data)
            setHats(data.hats)
        }
        getFetch()
    }, []);

    const deleteHat=async (id) => {
        const response= await fetch(`http://localhost:8090/api/hats/${id}/`, {
            method: 'DELETE',
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            }
        })
        const data = await response.json()
        console.log("hat deleted")
            setHats(
            hats.filter((hat) =>{
                return hat.id !== id;
            })
        )
    }
 
  return (
    <>

      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
            <th>Style</th>
                <th>Location</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Image</th>
                <th></th>
            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
              return (
                <tr key={hat.id}>
                  <td>{hat.style}</td>
                    <td>{hat.location}</td>
                    <td>{hat.fabric}</td>
                    <td>{hat.color}</td>
                    <td>
                        <img src={hat.image_url} style= {{width:"50px"}}/>
                    </td>
                    <td>
                        <button onClick={() => deleteHat(hat.id)}>Delete </button>
                    </td>

                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default HatList;
