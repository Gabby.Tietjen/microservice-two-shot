import React, { useState, useEffect } from 'react';

function ShoeForm() {
    const [shoes, setShoes] = useState([]);

    // Form Data
    const [manufacturer, setManufacturer] = useState("");
    const [model_name, setModelName] = useState("");
    const [color, setColor] = useState("");
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState("");
    const [image_url, setImageUrl] = useState("");


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        // console.log("response", response);

        if (response.ok) {
            const data = await response.json();
            // console.log("data", data);
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        //Collects Data from Shoe into an object 
        //that will be passed into our request's body
        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.bin = bin;
        data.image_url = image_url;

        const locationUrl = 'http://localhost:8080/api/shoes';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setManufacturer('');
            setModelName('');
            setColor('');
            setBin('');
            setImageUrl('');
        }
    }

    const handleManufacturerChange = async (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelNameChange = async (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleColorChange = async (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleBinChange = async (event) => {
        const value = event.target.value;
        setBin(value);
    }
    
    const handleImageUrlChange = async (event) => {
        const value = event.target.value;
        setImageUrl(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleManufacturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="name">Manufacturer</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={model_name} onChange={handleModelNameChange} placeholder="model_name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="mb-3">
                            <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.closet_name} value={bin.closet_name}>{bin.closet_name}</option>
                                    );
                                })}
                            </select>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={image_url} onChange={handleImageUrlChange} placeholder="image_url" required type="text" name="image_url" id="image_url" className="form-control" />
                            <label htmlFor="image_url">Image</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;