import React, {useState, useEffect} from 'react';


function HatForm () {
  const [hats, setHats] = useState([]);

  //Form Data
  const [fabric, setFabric] = useState("");
  const [style, setStyle] = useState("");
  const [color, setColor] = useState("");
  const [location, setLocation] = useState("");
  const [locations, setLocations] = useState([]);
  const [image_url, setImageUrl] = useState("");

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(()=> {
    fetchData();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();

    //Collects Data from State into an object 
    //that will be passed into our request's body
    const data = {};

    data.fabric = fabric;
    data.style = style;
    data.color = color;
    data.location = location;
    data.image_url = image_url;

    const locationUrl = 'http://localhost:8090/api/hats';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setFabric('');
      setStyle('');
      setColor('');
      setLocation('');
      setImageUrl('');
    }
  }

  const handleFabricChange = async (event) => {
    const value = event.target.value;
    setFabric(value);
  }

  const handleStyleChange = async (event) => {
    const value = event.target.value;
    setStyle(value);
  }
  
  const handleColorChange = async (event) => {
    const value = event.target.value;
    setColor(value);
  }
  
  const handleLocationChange = async (event) => {
    const value = event.target.value;
    setLocation(value);
  }
  const handleImageUrlChange = async (event) => {
    const value = event.target.value;
    setImageUrl(value);
  }
  
  return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-A-Hat">
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" value={style}/>
                <label htmlFor="color">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleImageUrlChange} placeholder="Image" required type="url" name="image_url" id="image_url" className="form-control" value={image_url}></textarea>
                  <label htmlFor="image_url">Image</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.closet_name} value={location.closet_name}>{location.closet_name}</option>
                    );
                  })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    
            );
        }

export default HatForm;