import React, { useState, useEffect } from "react";


function ShoeList(props) {
    const [shoes, setShoes] = useState([])

    useEffect(() => {
        function fetchShoes() {
            fetch("http://localhost:8080/api/shoes")
                .then(response => response.json())
                .then(data => {
                    console.log("shoedata", data)
                    setShoes(data.shoes)
                })
        }
        fetchShoes()
    }, []);

    const deleteShoe = (id) => {
        // console.log("id", id);
        fetch(`http://localhost:8080/api/shoes/${id}/`, {
            method: "DELETE",
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log("shoedeleted", data)
                setShoes(
                    shoes.filter((shoe) =>{
                        return shoe.id !== id;
                    })
                )
            })
    }
    return (
        <>
            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                            <th>Model Name</th>
                            <th>Color</th>
                            <th>Image</th>
                            <th>Bin</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {shoes.map(shoe => {
                            return (
                                <tr key={shoe.id}>
                                    <td>{shoe.manufacturer}</td>
                                    <td>{shoe.model_name}</td>
                                    <td>{shoe.color}</td>
                                    <td>
                                        <img src={shoe.image_url} alt={shoe.model_name} style={{ width: "50px" }} />
                                    </td>
                                    <td>{shoe.bin}</td>
                                    <td>
                                        <button onClick={() => deleteShoe(shoe.id)}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default ShoeList;