# Wardrobify

Team:

* Person 1 - Gabby Tietjen Hats
* Person 2 - Jennifer Tovar - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

We created a value object bin to create a connection from wardrobe to the shoes microservice.
Our model contains all the objects within the shoes. We were able to pull those objects out and create a form that showed all those objects and allowed a user to create a new shoe. Users were also able to delete a shoe from the bin.

## Hats microservice

We created a value object location to create bridge from wardrobe to the hats microservice.
Our model contains all the objects within hats.  We were able to pull those objects out and create a form that showed all those objects and allowed a user to create a new hat.
